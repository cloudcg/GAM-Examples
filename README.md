<h1>GAM Examples</h1>

<p>This is a collection of scripts / commands using <a href="https://github.com/jay0lee/GAM">GAM by Jay0Lee</a></p>

<p>To use them, simply follow the instructions on the <a href="https://github.com/jay0lee/GAM/wiki/">GAM Wiki</a> to install GAM and then you can use these files as either Batch, Powershell, Bash or Python scripts.</p>
<br />
<h2>Clearing_Print_Jobs</h2>
<p>This script clears print jobs with particular statuses and ages. For more information see : <a href="https://cloud-cg.com/google-cloud-print-and-gam-part-1/">cloud-cg.com/blog</a></p>

<h2>Printer_Status</h2>
<p>This script generates a list of offline Google Cloud Printers. For more information see : <a href="https://cloud-cg.com/google-cloud-print-and-gam-part-1/">cloud-cg.com/blog</a></p>